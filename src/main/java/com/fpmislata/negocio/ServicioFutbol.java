/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.fpmislata.negocio;

import com.fpmislata.datos.DAOException;
import com.fpmislata.datos.FutbolAleatorioDAO;
import com.fpmislata.datos.FutbolJDBCDAO;
import com.fpmislata.datos.FutbolTxtDAO;
import com.fpmislata.datos.FutbolXMLDAO;
import com.fpmislata.datos.FutbolXMLSaxDAO;
import com.fpmislata.datos.IFutbolDAO;
import com.fpmislata.entidades.Equipo;
import com.fpmislata.entidades.Jugador;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author rafael
 */

@SuppressWarnings("unused")
public class ServicioFutbol {
	private IFutbolDAO idao = null;

	public ServicioFutbol() {

	}

	public void elegirSistemaAlmacenamiento(int opcion) throws ServicioException, DAOException {

		if (opcion == 1) {
			idao = new FutbolTxtDAO();
		}
		if (opcion == 2) {

			idao = new FutbolAleatorioDAO();

		}
		if (opcion == 3) {

			idao = new FutbolXMLDAO();

		}
		if (opcion == 4) {

			idao = new FutbolXMLSaxDAO();

		}
		if (opcion == 5) {

			idao = new FutbolJDBCDAO();

		}

	}

	public void nuevoJugador(int id, String nombre, Date fechaNac, int idEquipo)
			throws ServicioException, DAOException {
		// El curso pasado en algunos ejemplos pasabamos desde la interfaz
		// gráfica la clase Jugador
		// ya formada, no es que estuviese mal, pero así en el nivel de arriba
		// no se tiene que proecupar de crear la
		// clase jugador, pasa datos simples y se olvida, ya se encargará el
		// nivel de abajo.
		// siguiendo con la misma filosofia pasamos la fecha como String, y así
		// las comprobaciones
		// las hacemos todas aquí, el nivel de arriba se encarga exclusivamente
		// de lo que le toca, si es la clase
		// controlador pues se encarga solo de redirigir a la vista que toca, si
		// es todo integrado en la interfaz de consoloa
		// como en este caso pues se encarga de todas sus funciones.
		Jugador j = new Jugador();
		if (id <= 0) {
			throw new ServicioException("El identificador debe ser mayor que 0");
		}
		if (idEquipo <= 0) {
			throw new ServicioException("El identificador de Equipo debe ser mayor que 0");
		}
		j.setId(id);
		j.setNombre(nombre);

		j.setFechaNacimiento(fechaNac);

		// Vamos a comprobar que no existe el jugador
		Jugador jPrueba = idao.obtenerJugador(j.getId());
		if (jPrueba != null) {
			throw new ServicioException("Ya existe un jugador con ese Id");
		}

		Equipo e = idao.obtenerEquipo(idEquipo);
		// En esta aplicación suponemos que todo jugador debe pertenecer a un
		// equipo
		if (e == null) {
			throw new ServicioException("No existe ning�n equipo con ese id");
		}
		j.setEquipo(e);

		idao.nuevoJugador(j);

	}

	public void nuevoEquipo(int id, String nombre, double presupuesto) throws ServicioException, DAOException {
		// Comprobamos que el equipo no existe

		Equipo e = idao.obtenerEquipo(id);
		if (e != null) {
			throw new ServicioException("Ya existe un equipo con ese Id");
		}
		e = new Equipo();
		if (id <= 0) {
			throw new ServicioException("El identificador debe ser mayor que 0");
		}
		e.setId(id);
		e.setNombre(nombre);
		e.setPresupuesto(presupuesto);
		idao.nuevoEquipo(e);

	}

	public List<Jugador> obtenerTodosJugadores() throws DAOException {

		List<Jugador> jugadores = idao.obtenerTodosJugadores();
		// si no hay nada devuelve una lista vacia
		if (jugadores == null) {
			jugadores = new ArrayList<Jugador>();
		}
		return jugadores;

	}

	public List<Equipo> obtenerTodosEquipos() throws DAOException {

		List<Equipo> equipos = idao.obtenerTodosEquipos();
		// si no hay nada devuelve una lista vacia
		if (equipos == null) {
			equipos = new ArrayList<Equipo>();
		}
		return equipos;

	}

	public void eliminarJugador(int idJugador) throws ServicioException, DAOException {

		idao.eliminarJugador(idJugador);
	}

}
