package com.fpmislata.datos;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import com.fpmislata.entidades.Equipo;
import com.fpmislata.entidades.Jugador;

public class FutbolTxtDAO implements IFutbolDAO {
	private String archivoJugadores = "almacenamiento/jugadores.txt";
	private String archivoEquipos = "almacenamiento/equipos.txt";
	
	public FutbolTxtDAO() throws DAOException {
		try{
		 Properties pro = new Properties();
		 pro.load(this.getClass().getResourceAsStream("/configuracion.properties"));
		 this.archivoJugadores=pro.getProperty("jugadoresTxt");
		 this.archivoEquipos=pro.getProperty("equiposTxt");
		}catch(IOException e){
			throw new DAOException("Ha habido un problema al obtener el archivo de configuracion",e);
		}
		
	}

	public void nuevoJugador(Jugador j) throws DAOException {
		List<Jugador> jugadores = null;
		jugadores = this.volcarTxtEnJugadores();
		jugadores.add(j);
		this.volcarJugadoresEnTxt(jugadores);

	}

	public void nuevoEquipo(Equipo e) throws DAOException {
		List<Equipo> equipos = null;
		equipos = this.volcarTxtEnEquipos();
		equipos.add(e);
		this.volcarEquiposEnTxt(equipos);

	}

	public void eliminarJugador(int id) throws DAOException { // TODO
		// TODO

	}

	public Jugador obtenerJugador(int idJugador) throws DAOException {
		Jugador ju = null;
		List<Jugador> jugadores = null;
		jugadores = this.volcarTxtEnJugadores();
		for (Jugador j : jugadores) {
			if (j.getId() == idJugador) {
				ju = j;
			}
		}
		return ju;
	}

	public Equipo obtenerEquipo(int idEquipo) throws DAOException {
		Equipo eq = null;
		List<Equipo> equipos = this.volcarTxtEnEquipos();
		for (Equipo e : equipos) {
			if (e.getId() == idEquipo) {
				eq = e;
			}
		}
		return eq;
	}

	public List<Jugador> obtenerTodosJugadores() throws DAOException {
		List<Jugador> jugadores=this.volcarTxtEnJugadores();
		Collections.sort(jugadores);

		return jugadores;
	}

	public List<Equipo> obtenerTodosEquipos() throws DAOException {
		List<Equipo> equipos= this.volcarTxtEnEquipos();
		Collections.sort(equipos);

		return equipos;
	}

	private List<Jugador> volcarTxtEnJugadores() throws DAOException {
		List<Jugador> jugadores = new ArrayList<Jugador>();

		File archivo = null;
		FileReader fr = null;
		BufferedReader br = null;

		List<Equipo> equipos = null;
		equipos = this.volcarTxtEnEquipos();

		try {

			archivo = new File(archivoJugadores);
			fr = new FileReader(archivo);
			br = new BufferedReader(fr);

			String linea;
			while ((linea = br.readLine()) != null) {
				Jugador j = new Jugador();
				String[] str = linea.split("#");
				String idStr = str[0];
				String nombre = str[1];
				String fechaNacStr = str[2];
				String equipoStr = str[3];

				// Transformamos al tipon de dato apropiado lo q no sea string

				int id = Integer.parseInt(idStr);
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				Date d = sdf.parse(fechaNacStr);
				int equipoIdent = Integer.parseInt(equipoStr);

				// Buscamos el equipo a aprtir de su id
				
				Equipo equipoJug = null;
				for (Equipo e : equipos) {
					if (equipoIdent == e.getId()) {
						equipoJug = e;
					}
				}

				// rellenamos el objeto

				j.setId(id);
				j.setNombre(nombre);
				j.setFechaNacimiento(d);
				j.setEquipo(equipoJug);

				// y lo a�adimos a la lista
				jugadores.add(j);

			}

			return jugadores;

		} catch (Exception e) {
			throw new DAOException(
					"Ha habido un problema al obtener los grupos desde el archivo de texto:",
					e);
		} finally {

			try {
				if (null != fr) {
					fr.close();
				}
			} catch (Exception e2) {
				throw new DAOException(
						"Ha habido un problema al cerrar el archivo de texto",
						e2);
			}
		}

	}

	private List<Equipo> volcarTxtEnEquipos() throws DAOException {
		List<Equipo> equipos = new ArrayList<Equipo>();
		File archivo = null;
		FileReader fr = null;
		BufferedReader br = null;

		try {

			archivo = new File(archivoEquipos);
			fr = new FileReader(archivo);
			br = new BufferedReader(fr);

			String linea;
			while ((linea = br.readLine()) != null) {
				Equipo e = new Equipo();
				String[] str = linea.split("#");
				String identEquipoStr = str[0];
				String nombreEquipo = str[1];
				String presupuestoStr = str[2];

				int ident = Integer.parseInt(identEquipoStr);
				double presupuesto = Double.parseDouble(presupuestoStr);
				e.setId(ident);
				e.setNombre(nombreEquipo);
				e.setPresupuesto(presupuesto);

				equipos.add(e);

			}

			return equipos;

		} catch (Exception e) {
			throw new DAOException(
					"Ha habido un problema al obtener los equipos desde el archivo de texto:",
					e);
		} finally {
			try {
				if (null != fr) {
					fr.close();
				}
			} catch (Exception e2) {
				throw new DAOException(
						"Ha habido un problema al cerrar el archivo de texto",
						e2);
			}
		}

	}

	private void volcarJugadoresEnTxt(List<Jugador> jugadores)
			throws DAOException {
		FileWriter fichero = null;
		PrintWriter pw = null;
		try {
			fichero = new FileWriter(archivoJugadores, false);
			pw = new PrintWriter(fichero);

			for (Jugador j : jugadores) {
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				String fechaStr = sdf.format(j.getFechaNacimiento());
				String cadena = j.getId() + "#" + j.getNombre() + "#"
						+ fechaStr + "#" + j.getEquipo().getId();
				pw.println(cadena);

			}

		} catch (Exception e) {
			throw new DAOException(
					"Ha habido un problema al guardar los jugadores en el archivo de texto:",
					e);
		} finally {
			try {

				if (null != fichero)
					fichero.close();
			} catch (Exception e2) {
				throw new DAOException(
						"Ha habido un problema al cerrar el archivo de texto",
						e2);
			}
		}

	}

	private void volcarEquiposEnTxt(List<Equipo> equipos) throws DAOException {
		FileWriter fichero = null;
		PrintWriter pw = null;
		try {
			fichero = new FileWriter(archivoEquipos);
			pw = new PrintWriter(fichero);

			for (Equipo e : equipos) {

				String cadena = e.getId() + "#" + e.getNombre() + "#"
						+ e.getPresupuesto();
				pw.println(cadena);

			}

		} catch (Exception e) {
			throw new DAOException(
					"Ha habido un problema al guardar los grupos en el archivo de texto:",
					e);
		} finally {
			try { // Nuevamente aprovechamos el finally para

				// asegurarnos que se cierra el fichero. if (null != fichero)
				fichero.close();
			} catch (Exception e2) {
				throw new DAOException(
						"Ha habido un problema al cerrar el archivo de texto",
						e2);
			}
		}
	}

}
