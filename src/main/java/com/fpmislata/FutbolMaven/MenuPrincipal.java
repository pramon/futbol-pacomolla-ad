/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.fpmislata.FutbolMaven;

import com.fpmislata.datos.DAOException;
import com.fpmislata.entidades.Equipo;
import com.fpmislata.entidades.Jugador;
import com.fpmislata.negocio.ServicioException;
import com.fpmislata.negocio.ServicioFutbol;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author rafael
 */

// Esta es la típica clase Menu Principal de una aplicación por consola.
// ¿De que se encarga? de DOS cosas
// 1.De dirigir el flujo de la aplicación dependiendiendo de lo que introduzca
// el usuario
// 2.de recoger los datos e imprimir por consola
// En el MVC esta clase engloba las funciones de Vista y de Controlador.
// Para una aplicación por consola sencilla como esta puede valer pero
// imaginaos
// Una clase Menu principal que tuvise que generar las vistas y dirigir el flujo
// de un sitio Web, vamos una locura. por ese motivo estas funciones se separan
public class MenuPrincipal {
	ServicioFutbol sf = null;

	public MenuPrincipal() {
		sf = new ServicioFutbol();
	}
	 public void elegirSistemaAlmacenamiento(){
	    	try {
	    		 int opcion=-1;
	    	        while(opcion!=0){
	    	            System.out.println("Introduzca El tipo de sistema de almacenamiento");
	    	            System.out.println("1.Archivo de texto");
	    	            System.out.println("2.Archivo Binario");
	    	            System.out.println("3.Archivo XML usando DOM");
	    	            System.out.println("4.Archivo XML usando SAX");
	    	            System.out.println("5.Base de datos relacional usando JDBC");
	    	            
	    	            System.out.println("0.Salir");
	    	            Scanner sc=new Scanner(System.in);
	    	            opcion=sc.nextInt();
	    	           
	    	            if(opcion>0 && opcion <6){
	    	                sf.elegirSistemaAlmacenamiento(opcion);
	    	                this.iniciarAplicacion();
	    	            }
	    	            if(opcion>=6){
	    	            	System.out.println("Opcion No valida");
	    	            
	    	            }
	    	           
	    	            if(opcion==0){
	    	            	System.out.println("Aplicacion FUTBOL cerrada");
	    	            
	    	            }
	    	    }
	    	}catch(InputMismatchException ex2){
	             System.out.println("Debe introducir un valor numerico");
	            
	 			 System.out.println("");;
	             this.elegirSistemaAlmacenamiento();
	             
	         }
	    	catch (DAOException e2) {
				System.out.println("Hubo un problema al iniciar el sistema de almacenamiento: ");
				e2.printStackTrace(System.out);
				System.out.println("");
				this.elegirSistemaAlmacenamiento();
				
				//e.printStackTrace();
			}catch (Exception e) {
				System.out.println("Error inesperado: ");
				e.printStackTrace(System.out);
				System.out.println("");
				this.elegirSistemaAlmacenamiento();
				
				//e.printStackTrace();
			}
	    	
	    	
	    }

	@SuppressWarnings("resource")
	public void iniciarAplicacion() {
		try {
			int opcion = -1;
			while (opcion != 0) {
				System.out.println("Introduzca la operaci�n");
				System.out.println("1.Nuevo Jugador");
				System.out.println("2.Nuevo Equipo");
				System.out.println("3.Ver todos los Jugadores");
				System.out.println("4.Ver todos los Equipos");
				System.out.println("5.Eliminar Jugador");
				// System.out.println("6.Eliminar Equipo");
				// Vamos a quitar esta opción, al eliminar un equipo ¿Que
				// hacemos con sus jugadores
				// como visteis en BD hay varias opciones
				// 1.Eliminar todos sus jugadores
				// 2.Poner los jugadores con equipo == null, esto no es posible
				// según está especificada la aplicacion
				// 3. Tener un equipo por defecto y asignarselos a este, no se
				// podrá borrar el equipo por defecto
				// 4.No permitido, no se pueden borrar equipos con jugadores
				// asignados, pero sí los equipos que no los tengan
				// Vamos a simplificar y simplemente no vamos a implementar la
				// opcion, como ejercicio de ampliación podeis hacerlo
				// siguiendo alguno de los criterios arriba descritos
				System.out.println("0.Salir");
				Scanner sc = new Scanner(System.in);
				opcion = sc.nextInt();

				if (opcion == 1) {
					this.nuevoJugador();
				}
				if (opcion == 2) {
					this.nuevoEquipo();
				}
				if (opcion == 3) {
					this.verTodosJugadores();
				}
				if (opcion == 4) {
					this.verTodosEquipos();
				}
				if (opcion == 5) {

					// realizar como ejercicio
					System.out.println("Opci�n No implementada");
				}
				if (opcion == 0) {
					//System.out.println("APLICACION FUTBOL CERRADA");
					System.out.println("");
					System.out.println("");
				}
			}
		} catch (InputMismatchException imEx2) {
			System.out.println("Debe introducir un valor num�rico");
			System.out.println("");
			this.iniciarAplicacion();

		} catch (Exception e) {
			System.out.println("ERROR Inesperado: ");
			e.printStackTrace(System.out);
			System.out.println("");
		} 

	}

	@SuppressWarnings("resource")
	public void nuevoJugador() throws DAOException, ServicioException {
		try {
			System.out.println("Introduzca el id");
			Scanner sc = new Scanner(System.in);
			int id = sc.nextInt();
			System.out.println("Introduzca el nombre del jugador");
			sc = new Scanner(System.in);
			String nombre = sc.nextLine();
			System.out.println("Introduzca la fecha de nacimiento(dd/MM/yyyy)");
			sc = new Scanner(System.in);
			String fechaStr = sc.nextLine();
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			Date fechaNac = null;

			fechaNac = sdf.parse(fechaStr);

			System.out.println("Introduzca el id de su equipo");
			sc = new Scanner(System.in);
			int idEquipo = sc.nextInt();
			sf.nuevoJugador(id, nombre, fechaNac, idEquipo);

			/*
			 * } catch (NullPointerException ex) { System.out .println(
			 * "Error: No se ha inicializado correctamente el dispositivo de almacenamiento"
			 * + ex.getMessage()); System.out.println("");
			 */

		} catch (InputMismatchException imEx2) {
			System.out.println("Debe introducir un valor num�rico");
			System.out.println("");

		} catch (ParseException pEx) {
			System.out.println("El formato de fecha no es v�lido");
			System.out.println("");
		} 
		

	}

	@SuppressWarnings("resource")
	public void nuevoEquipo() throws DAOException ,ServicioException{
		try {
			System.out.println("Introduzca el id");
			Scanner sc = new Scanner(System.in);
			int id = sc.nextInt();
			System.out.println("Introduzca el nombre del equipo");
			sc = new Scanner(System.in);
			String nombre = sc.nextLine();
			System.out.println("Introduzca el presupuesto");
			sc = new Scanner(System.in);
			double presupuesto = sc.nextDouble();

			sf.nuevoEquipo(id, nombre, presupuesto);

			

		} catch (InputMismatchException imEx2) {
			System.out.println("Debe introducir un valor num�rico");
			System.out.println("");

		} 
		
		

	}

	public void verTodosEquipos()  throws DAOException {
		
			List<Equipo> equipos = sf.obtenerTodosEquipos();
			System.out.println("_______EQUIPOS______");
			System.out.println("");
			for (Equipo e : equipos) {
				System.out.println(e.getId() + "  " + e.getNombre() + "  "
						+ e.getPresupuesto());
			}
			System.out.println();

			
		

	}

	public void verTodosJugadores() throws DAOException {
	
			List<Jugador> jugadores = sf.obtenerTodosJugadores();
			System.out.println("_______Jugadores______");
			System.out.println("");
			for (Jugador j : jugadores) {
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				String fecha = sdf.format(j.getFechaNacimiento());
				System.out.println(j.getId() + "  " + j.getNombre() + "  "
						+ fecha + "   " + j.getEquipo().getNombre());

			}
			System.out.println();


		
	}

}
