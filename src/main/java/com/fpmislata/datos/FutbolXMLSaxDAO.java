package com.fpmislata.datos;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;





import java.util.Properties;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;

import com.fpmislata.entidades.Equipo;
import com.fpmislata.entidades.Jugador;

public class FutbolXMLSaxDAO implements IFutbolDAO {

	private String archivoJugadores = null;
	private String archivoEquipos = "equiposSax.xml";
	

	
	public FutbolXMLSaxDAO() throws DAOException {
		try{
			 Properties pro = new Properties();
			 pro.load(this.getClass().getResourceAsStream("/configuracion.properties"));
			 this.archivoJugadores=pro.getProperty("jugadoresXMLSax");
			 this.archivoEquipos=pro.getProperty("equiposXMLSax");
			}catch(Exception e){
				throw new DAOException("Ha habido un problema al obtener el archivo de configuracion",e);
			}
	}


	public void nuevoJugador(Jugador j) throws DAOException {
		List<Jugador> jugadores = this.ObtenerJugadoresDeXML();
		jugadores.add(j);
		this.volcarJugadoresEnXML(jugadores);

	}

	
	public void nuevoEquipo(Equipo e) throws DAOException {
		List<Equipo> equipos = null;
		equipos = this.ObtenerEquiposDeXML();
		equipos.add(e);
		this.volcarEquiposEnXML(equipos);

	}

	
	public void eliminarJugador(int id) throws DAOException {
		List<Jugador> jugadores= this.obtenerTodosJugadores();
		Jugador elim=null;
		for(Jugador j :jugadores){
			if(j.getId()==id){
				elim=j;
			}
		}
		jugadores.remove(elim);
		this.volcarJugadoresEnXML(jugadores);

	}

	
	public Jugador obtenerJugador(int idJugador) throws DAOException {
		Jugador ju = null;
		List<Jugador> jugadores = null;
		jugadores = this.ObtenerJugadoresDeXML();
		for (Jugador j : jugadores) {
			if (j.getId() == idJugador) {
				ju = j;
			}
		}
		return ju;
	}

	
	public Equipo obtenerEquipo(int idEquipo) throws DAOException {
		Equipo eq = null;
		List<Equipo> equipos = this.ObtenerEquiposDeXML();
		for (Equipo e : equipos) {
			if (e.getId() == idEquipo) {
				eq = e;
			}
		}
		return eq;
	}

	
	public List<Jugador> obtenerTodosJugadores() throws DAOException {
		try {
			return this.ObtenerJugadoresDeXML();
		} catch (Exception e) {
			throw new DAOException(
					"Ha habido un pronblema al obtener los jugadores del XML por sax\n",e);
		}
	}


	public List<Equipo> obtenerTodosEquipos() throws DAOException {
		try {
			return this.ObtenerEquiposDeXML();
		} catch (Exception e) {
			throw new DAOException(
					"Ha habido un pronblema al obtener los equipos del XML por sax\n",e);
		}
	}

	
	public void finalizar() throws DAOException {
		// TODO Auto-generated method stub

	}

	// No tenemos una forma sencilla para escribir un xml usando sax lo hacemos
	// con ficheros de texto directamente
	private void volcarEquiposEnXML(List<Equipo> equipos) throws DAOException {
		FileWriter fichero = null;
		PrintWriter pw = null;

		try {
			fichero = new FileWriter(archivoEquipos, false);
			pw = new PrintWriter(fichero);
			pw.println("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>");
			pw.println("<Equipos>");
			for (Equipo e : equipos) {
				pw.println("<equipo>");
				pw.println("      <id>" + e.getId() + "</id>");
				pw.println("      <nombre>" + e.getNombre() + "</nombre>");
				pw.println("      <presupuesto>" + e.getPresupuesto()
						+ "</presupuesto>");
				pw.println("</equipo>");

			}
			pw.println("</Equipos>");

		} catch (Exception e) {
			throw new DAOException(
					"Ha habido un problema al guardar los equipos en el archivo XML:",e);
		} finally {
			try {

				if (null != fichero)
					fichero.close();
			} catch (Exception e2) {
				throw new DAOException(
						"Ha habido un problema al cerrar el archivo de texto",e2);
			}
		}

	}

	private void volcarJugadoresEnXML(List<Jugador> jugadores) throws DAOException {
		FileWriter fichero = null;
		PrintWriter pw = null;

		try {
			fichero = new FileWriter(archivoJugadores, false);
			pw = new PrintWriter(fichero);
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			pw.println("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>");
			pw.println("<Jugadores>");
			for (Jugador j : jugadores) {

				pw.println("<jugador>");
				pw.println("      <id>" + j.getId() + "</id>");
				pw.println("      <nombre>" + j.getNombre() + "</nombre>");
				String fechaStr = sdf.format(j.getFechaNacimiento());

				pw.println("      <fechaNacimiento>" + fechaStr
						+ "</fechaNacimiento>");
				pw.println("      <equipo>" + j.getEquipo().getId()
						+ "</equipo>");
				pw.println("</jugador>");

			}
			pw.println("</Jugadores>");

		} catch (Exception e) {
			throw new DAOException(
					"Ha habido un problema al guardar los jugadores en el archivo XML:",e);
		} finally {
			try {

				if (null != fichero)
					fichero.close();
			} catch (Exception e2) {
				throw new DAOException("Ha habido un problema al cerrar el archivo de texto",e2);
			}
		}

	}

	private List<Jugador> ObtenerJugadoresDeXML() throws DAOException {
		List<Jugador> jugadores = null;
		try {
			XMLReader procesadorXML = XMLReaderFactory.createXMLReader();
			GestionContenidoJugador gestor = new GestionContenidoJugador();
			procesadorXML.setContentHandler(gestor);
			InputSource fileXML = new InputSource(archivoJugadores);
			procesadorXML.parse(fileXML);
			jugadores = gestor.getJugadores();

		} catch (SAXParseException spe) {
			if (spe.getLineNumber() == 1) {
				// El archivo está vacio devolvemos la lista sin nada y ya esta
				jugadores = new ArrayList<Jugador>();
				return jugadores;
			}
			throw new DAOException("El archivo XML de jugadores está corrupto ",spe);

		}

		catch (Exception se) {
			throw new DAOException(
					"Ha habido un problema al obtener el procesador de Sax",se);
		}

		return jugadores;
	}

	private List<Equipo> ObtenerEquiposDeXML() throws DAOException {
		List<Equipo> equipos = null;
		try {
			XMLReader procesadorXML = XMLReaderFactory.createXMLReader();
			GestionContenidoEquipo gestor = new GestionContenidoEquipo();
			procesadorXML.setContentHandler(gestor);
			InputSource fileXML = new InputSource(archivoEquipos);
			procesadorXML.parse(fileXML);
			equipos = gestor.getEquipos();

		} catch (SAXParseException spe) {
			if (spe.getLineNumber() == 1) {
				// El archivo está vacio devolvemos la lista sin nada y ya esta
				equipos = new ArrayList<Equipo>();
				return equipos;
			}
			throw new DAOException("    El archivo XML de equipos est� corrupto ",spe);

		} catch (Exception se) {

			throw new DAOException(
					"Ha habido un problema al obtener el procesador de Sax",se);
		}

		return equipos;

	}

	// Aqui estan las clases privadas de Sax y la enumeraci�n que vamos a usar
	// Tambi�n se podria hacer sin enumeraci�n usando un String cuyos valores
	// siempre son los mismos
	private enum TipoNodo {
		EQUIPOS, EQUIPO, ID, NOMBRE, PRESUPUESTO, JUGADORES, JUGADOR, FECHANACIMIENTO, IDEQUIPO
	};

	private class GestionContenidoEquipo extends DefaultHandler {

		private List<Equipo> equipos = null;
		private Equipo equipoNodo = null;
		private TipoNodo tipo = null;

		public GestionContenidoEquipo() {
			super();

		}

		public void startDocument() {
			// System.out.println("Inicio");
			equipos = new ArrayList<Equipo>();

		}

		public void endDocument() {
		//	System.out.println("Fin");

		}

		public void startElement(String uri, String nombre, String nombreC,
				Attributes atts) {

		//	System.out.println("elemento inicio, uri:" + uri + " nombre: "
		//			+ nombre + " nombreC: " + nombreC + " Atrib "
		//			+ atts.getValue(0));
			if (nombre.equals("Equipos")) {
				tipo = TipoNodo.EQUIPOS;
			}
			if (nombre.equals("equipo")) {
				tipo = TipoNodo.EQUIPO;
				equipoNodo = new Equipo();
			}
			if (nombre.equals("id")) {
				tipo = TipoNodo.ID;
			}
			if (nombre.equals("nombre")) {
				tipo = TipoNodo.NOMBRE;
			}
			if (nombre.equals("presupuesto")) {
				tipo = TipoNodo.PRESUPUESTO;
			}

		}

		public void endElement(String uri, String nombre, String nombreC) {
		//	System.out.println("elemento inicio, uri:" + uri + " nombre: "
		//			+ nombre + " nombreC: " + nombreC + " Atrib ");
			if (nombre.equals("equipo")) {
				setTipo(TipoNodo.EQUIPO);
				equipos.add(equipoNodo);
			}
			tipo = null;
		}

		public void characters(char[] ch, int inicio, int longitud) {

			String car = new String(ch, inicio, longitud);
	//		System.out.println("caracteres: " + car);
			if (tipo == TipoNodo.ID) {
				int id = Integer.parseInt(car);
				equipoNodo.setId(id);
			}
			if (tipo == TipoNodo.NOMBRE) {

				equipoNodo.setNombre(car);
			}
			if (tipo == TipoNodo.PRESUPUESTO) {
				double prep = Double.parseDouble(car);
				equipoNodo.setPresupuesto(prep);
			}

		}

		public List<Equipo> getEquipos() {
			return equipos;
		}

		@SuppressWarnings("unused")
		public void setEquipos(List<Equipo> equipos) {
			this.equipos = equipos;
		}

		@SuppressWarnings("unused")
		public TipoNodo getTipo() {
			return tipo;
		}

		public void setTipo(TipoNodo tipo) {
			this.tipo = tipo;
		}

	}

	private class GestionContenidoJugador extends DefaultHandler {

		private List<Jugador> jugadores = null;
		private Jugador jugadorNodo = null;
		private TipoNodo tipo = null;

		public GestionContenidoJugador() {
			super();

		}

		public void startDocument() {
		//	 System.out.println("Inicio");
			jugadores = new ArrayList<Jugador>();

		}

		public void endDocument() {
			//System.out.println("Fin");

		}

		public void startElement(String uri, String nombre, String nombreC,
				Attributes atts) {

	//		System.out.println("elemento inicio, uri:" + uri + " nombre: "
	//				+ nombre + " nombreC: " + nombreC + " Atrib "
	//				+ atts.getValue(0));
			if (nombre.equals("jugadores")) {
				tipo = TipoNodo.JUGADORES;
			}
			if (nombre.equals("jugador")) {
				tipo = TipoNodo.JUGADOR;
				jugadorNodo = new Jugador();
			}
			if (nombre.equals("id")) {
				tipo = TipoNodo.ID;
			}
			if (nombre.equals("nombre")) {
				tipo = TipoNodo.NOMBRE;
			}
			if (nombre.equals("fechaNacimiento")) {
				tipo = TipoNodo.FECHANACIMIENTO;
			}
			if (nombre.equals("equipo")) {
				tipo = TipoNodo.IDEQUIPO;
			}

		}

		public void endElement(String uri, String nombre, String nombreC) {
	//		System.out.println("elemento inicio, uri:" + uri + " nombre: "
	//				+ nombre + " nombreC: " + nombreC + " Atrib ");
			if (nombre.equals("jugador")) {
				tipo = TipoNodo.JUGADOR;
				jugadores.add(jugadorNodo);
			}
			tipo = null;
		}

		public void characters(char[] ch, int inicio, int longitud) {

			String car = new String(ch, inicio, longitud);
	//		System.out.println("caracteres: " + car);
			if (tipo == TipoNodo.ID) {
				int id = Integer.parseInt(car);
				jugadorNodo.setId(id);
			}
			if (tipo == TipoNodo.NOMBRE) {

				jugadorNodo.setNombre(car);
			}
			if (tipo == TipoNodo.FECHANACIMIENTO) {
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				Date d = null;
				try {
					d = sdf.parse(car);
				} catch (ParseException e) {
					// Aqui no deberia entrar nunca si lo hace es porque el
					// archivo esta corrupto
					e.printStackTrace();
				}
				jugadorNodo.setFechaNacimiento(d);
			}
			if (tipo == TipoNodo.IDEQUIPO) {
				try {
				int idEquipo = Integer.parseInt(car);
				FutbolXMLSaxDAO fdao = new FutbolXMLSaxDAO();
				
					Equipo e = fdao.obtenerEquipo(idEquipo);
					jugadorNodo.setEquipo(e);
				} catch (Exception e) {
					// Aqui no deberia entrar nunca si lo hace es porque el
					// archivo esta corrupto
					e.printStackTrace();
				}

			}

		}

		public List<Jugador> getJugadores() {
			return jugadores;
		}

		
	}
}
