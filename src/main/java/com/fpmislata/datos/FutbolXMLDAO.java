package com.fpmislata.datos;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import com.fpmislata.entidades.Equipo;
import com.fpmislata.entidades.Jugador;

public class FutbolXMLDAO implements IFutbolDAO {
	String archivoJugadores=null;
	String archivoEquipos=null;
	

	public FutbolXMLDAO() throws DAOException {
		try{
			 Properties pro = new Properties();
			 pro.load(this.getClass().getResourceAsStream("/configuracion.properties"));
			 this.archivoJugadores=pro.getProperty("jugadoresXML");
			 this.archivoEquipos=pro.getProperty("equiposXML");
			}catch(Exception e){
				throw new DAOException("Ha habido un problema al obtener el archivo de configuracion",e);
			}

	}


	public void nuevoJugador(Jugador j) throws DAOException {
	 List<Jugador> jugadores=this.recuperarJugadoresXML();
	 jugadores.add(j);
	 this.guardarJugadoresXML(jugadores);

	}

	
	public void nuevoEquipo(Equipo e) throws DAOException {
	List<Equipo> equipos= this.recuperarEquiposXML();
	equipos.add(e);
	this.guardarEquiposXML(equipos);    

	}

	
	public void eliminarJugador(int id) throws DAOException {
		List<Jugador> jugadores= this.obtenerTodosJugadores();
		Jugador elim=null;
		for(Jugador j :jugadores){
			if(j.getId()==id){
				elim=j;
			}
		}
		jugadores.remove(elim);
		this.guardarJugadoresXML(jugadores);

	}

	
	public Jugador obtenerJugador(int idJugador) throws DAOException {
		List<Jugador> jugadores=this.recuperarJugadoresXML();
		Jugador j=null;
		for(Jugador busca :jugadores){
			if(busca.getId()==idJugador){
				j=busca;
			}
		}
		return j;
	}

	
	public Equipo obtenerEquipo(int idEquipo) throws DAOException {
		List<Equipo> equipos= this.recuperarEquiposXML();
		Equipo e= null;
		for(Equipo busca :equipos){
			if(busca.getId()==idEquipo){
				e=busca;
			}
		}
		return e;
		
	}

	
	public List<Jugador> obtenerTodosJugadores() throws DAOException {
		
		return this.recuperarJugadoresXML();
	}

	
	public List<Equipo> obtenerTodosEquipos() throws DAOException {
		return this.recuperarEquiposXML();
	}

	
	public void finalizar() {
		// TODO Auto-generated method stub

	}

	

	private void guardarEquiposXML(List<Equipo> equipos) throws DAOException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

		try {
			DocumentBuilder builder = factory.newDocumentBuilder();
			DOMImplementation implementation = builder.getDOMImplementation();
			Document document = implementation.createDocument(null, "Equipos",
					null);
			document.setXmlVersion("1.0"); // asignamos la version de nuestro
											// XML

			for (Equipo e : equipos) {

				Element raiz = document.createElement("equipo"); // nodo equipo
																	
				document.getDocumentElement().appendChild(raiz);
				CrearElemento("id", Integer.toString(e.getId()), raiz,
						document); // a�adir ID
				CrearElemento("nombre", e.getNombre(), raiz, document); // nombre
				CrearElemento("presupuesto",
						Double.toString(e.getPresupuesto()), raiz,
						document); // presupuesto

			}
			
			Source source = new DOMSource(document);
		    Result result = new StreamResult(new java.io.File(archivoEquipos));        
		    Transformer transformer = TransformerFactory.newInstance().newTransformer();
		    transformer.transform(source, result);
		} catch (Exception ex) {
			throw new DAOException("No se ha podido insertar el equipo en el XML",ex);
		}
	}
	private List<Equipo> recuperarEquiposXML() throws DAOException{
		File file = new File(archivoEquipos);
		List<Equipo> equipos= new ArrayList<Equipo>();
		try{
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document document = db.parse(file);
		document.getDocumentElement().normalize();
		
		NodeList nodeListEquipos = document.getElementsByTagName("equipo");

		  for (int i = 0; i < nodeListEquipos.getLength(); i ++) {

		    Node nodo = nodeListEquipos.item(i);

		    if (nodo.getNodeType() == Node.ELEMENT_NODE) {

	            Element elemento = (Element) nodo;
	            String idStr=getNodo("id",elemento);
	            int id=Integer.parseInt(idStr);
	            String nombre=getNodo("nombre",elemento);
	            String presupuestoStr=getNodo("presupuesto",elemento);
	            double presupuesto= Double.parseDouble(presupuestoStr);
	            
	            Equipo e= new Equipo();
	            e.setId(id);
	            e.setNombre(nombre);
	            e.setPresupuesto(presupuesto);
	            equipos.add(e);

	            

		    }
	    }
		  return equipos;
		
		
		} catch (Exception ex) {
			throw new DAOException("No se ha podido leer el equipo desde el XML",ex);
		}
	}
	private void guardarJugadoresXML(List<Jugador> jugadores) throws DAOException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

		try {
			DocumentBuilder builder = factory.newDocumentBuilder();
			DOMImplementation implementation = builder.getDOMImplementation();
			Document document = implementation.createDocument(null, "Jugadores",
					null);
			document.setXmlVersion("1.0"); // asignamos la version de nuestro
											// XML

			for (Jugador j : jugadores) {

				Element raiz = document.createElement("jugador"); // nodo equipo
																	
				document.getDocumentElement().appendChild(raiz);
				CrearElemento("id", Integer.toString(j.getId()), raiz,
						document); // a�adir ID
				CrearElemento("nombre", j.getNombre(), raiz, document); // nombre
				SimpleDateFormat sdf= new SimpleDateFormat("dd/MM/yyyy");
		        String fechaStr;
		        
		        fechaStr=sdf.format(j.getFechaNacimiento());
				
				CrearElemento("fechaNacimiento",fechaStr, raiz,document); //fechanacimiento
				CrearElemento("equipo",Integer.toString(j.getEquipo().getId()), raiz,document); //fechanacimiento

			}
			
			Source source = new DOMSource(document);
		    Result result = new StreamResult(new java.io.File(archivoJugadores));        
		    Transformer transformer = TransformerFactory.newInstance().newTransformer();
		    transformer.transform(source, result);
		} catch (Exception ex) {
			throw new DAOException("No se ha podido insertar el jugador en el XML",ex);
		}
	}
	private List<Jugador> recuperarJugadoresXML() throws DAOException{
		File file = new File(archivoJugadores);
		List<Equipo> equipos= this.recuperarEquiposXML();
		List<Jugador> jugadores= new ArrayList<Jugador>();
		try{
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document document = db.parse(file);
		document.getDocumentElement().normalize();
		
		NodeList nodeListJugadores = document.getElementsByTagName("jugador");

		  for (int i = 0; i < nodeListJugadores.getLength(); i ++) {

		    Node nodo = nodeListJugadores.item(i);

		    if (nodo.getNodeType() == Node.ELEMENT_NODE) {

	            Element elemento = (Element) nodo;
	            
	            String idStr=getNodo("id",elemento);
	            int id=Integer.parseInt(idStr);
	            
	            String nombre=getNodo("nombre",elemento);
	            
	            String fechaNacStr=getNodo("fechaNacimiento",elemento);
	            SimpleDateFormat sdf= new SimpleDateFormat("dd/MM/yyyy");
	            Date fechaNac= sdf.parse(fechaNacStr);
	            
	            String idEqStr=getNodo("equipo",elemento);
	            int idEquipo=Integer.parseInt(idEqStr);
	            Equipo e=null;
	            for(Equipo busca :equipos){
	            	if(busca.getId()==idEquipo){
	            		e=busca;
	            	}
	            }
	            if(e==null){
	            	throw new Exception("Error base de datos XML inconsistente");
	            }
	            
	            Jugador j= new Jugador();
	            j.setId(id);
	            j.setNombre(nombre);
	            j.setFechaNacimiento(fechaNac);
	            j.setEquipo(e);
	           
	            jugadores.add(j);

	            

		    }
	    }
		  return jugadores;
		
		
		} catch (Exception ex) {
			throw new DAOException("No se ha podido leer el jugador desde el XML",ex);
		}
	}
	
	private static void CrearElemento(String dato, String valor, Element raiz,
			Document document) {

		Element elem = document.createElement(dato); // creamos hijo
		Text text = document.createTextNode(valor); // damos valor
		raiz.appendChild(elem); // pegamos el elemento hijo a la raiz
		elem.appendChild(text); // pegamos el valor

	}
	private static String getNodo(String etiqueta, Element elem)
	 {
		  NodeList nodo= elem.getElementsByTagName(etiqueta).item(0).getChildNodes();
		  Node valornodo = (Node) nodo.item(0);
		  return valornodo.getNodeValue();
	 }

}
