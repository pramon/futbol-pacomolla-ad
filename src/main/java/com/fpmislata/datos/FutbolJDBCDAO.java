/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fpmislata.datos;

import com.fpmislata.entidades.Equipo;
import com.fpmislata.entidades.Jugador;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author alumno
 */
public class FutbolJDBCDAO implements IFutbolDAO {
	private String bd = null;
	private String login = null;
	private String password = null;
	private String url =null;
    
    static String getAllEquipos="SELECT * FROM equipos;";
    static String insertEquipo="INSERT INTO equipos(id,nombre,presupuesto) VALUES (?,?,?);";
    static String getEquipo="SELECT * FROM equipos WHERE id=?;";
   
    
    static String getAllJugadores="SELECT * FROM jugadores;";
    static String getJugador="SELECT * FROM jugadores WHERE id=?;";
    static String insertJugador="INSERT INTO jugadores(id,nombre,fechaNacimiento,equipo) VALUES (?,?,?,?);";
    static String deleteJugador="DELETE FROM jugadores WHERE id=?;";

    public FutbolJDBCDAO() throws DAOException {
    	
    	
    	
    	 Connection conn = null;

         try {
        	 
        	 Properties pro = new Properties();
    		 pro.load(this.getClass().getResourceAsStream("/configuracion.properties"));
    		 this.bd=pro.getProperty("bdJDBC");
    		 this.login=pro.getProperty("loginJDBC");
    		 this.password=pro.getProperty("passwordJDBC");
    		 this.url= "jdbc:mysql://localhost/" + bd;

             Class.forName("com.mysql.jdbc.Driver").newInstance();

             conn = DriverManager.getConnection(url, login, password);

             if (conn != null) {
                 
                 conn.close();
             }

         } catch (InstantiationException ex) {
        	 throw new DAOException("Hubo un problema al iniciar la base de datos: ",ex);
         } catch (IllegalAccessException iae) {
        	 throw new DAOException("Hubo un problema al acceder a la base de datos.",iae);
         } catch (SQLException sqle) {
        	 throw new DAOException("Hubo un problema al intentar conectarse con la base de datos ",sqle);
             
         } catch (ClassNotFoundException cnfe) {
        	 throw new DAOException("No se encuentra el driver JDBC.",cnfe);
         } catch (IOException ioe) {
        	 throw new DAOException("Error al accerder al archivo de configuración: ",ioe);
		}
	}

	
    public void nuevoJugador(Jugador j) throws DAOException {
		//Añade a la BD el jugador que se le pasa como parametro, en este momento ya se ha comprobado
		// que el equipo existe 
        Connection conn = null;
        PreparedStatement ps = null;
        
        try {
            conn = DriverManager.getConnection(url, login, password);

            ps = conn.prepareStatement(insertJugador);
            ps.setInt(1, j.getId());
            ps.setString(2,j.getNombre());
            ps.setDate(3, new java.sql.Date(j.getFechaNacimiento().getTime()));
            ps.setInt(4, j.getEquipo().getId());
            
            
            @SuppressWarnings("unused")
			int afectadas = ps.executeUpdate();
            //Este entero no lo vamos a usar pero devuelve el número de flas aceptadas
            //En otras ocasiones nos puede ser útil, aquí siempre debe devolver 1
            
            

        } catch (Exception ex) {
        	throw new DAOException("Ha habido un problema al insertar el jugador: ",ex);
        } finally {
            try {
                ps.close();
                conn.close();
            } catch (SQLException sqlex) {
            	throw new DAOException("Error al cerrar la base de datos",sqlex);
            }

        }
    }

    
    public void nuevoEquipo(Equipo e) throws DAOException {
    	//Añade a la BD el equipo que se le pasa como parametro, 
    	        Connection conn = null;
    	        PreparedStatement ps = null;
    	        
    	        try {
    	            conn = DriverManager.getConnection(url, login, password);

    	            ps = conn.prepareStatement(insertEquipo);
    	            ps.setInt(1, e.getId());
    	            ps.setString(2,e.getNombre());
    	            ps.setDouble(3, e.getPresupuesto());
    	            
    	            
    	            ps.executeUpdate();
    	            
    	            

    	        } catch (Exception ex) {
    	        	throw new DAOException("Ha habido un problema al insertar el equipo: ",ex);
    	        } finally {
    	            try {
    	                ps.close();
    	                conn.close();
    	            } catch (SQLException ex) {
    	            	throw new DAOException("Error al cerrar la base de datos",ex);
    	            }

    	        }
    }

    
    public Jugador obtenerJugador(int idJugador) throws DAOException {
    	//Obtiene de la BD el jugador con id 
        Jugador j= new Jugador();
        Connection conn = null;
        PreparedStatement ps = null;
        
        try {
            conn = DriverManager.getConnection(url, login, password);

            ps = conn.prepareStatement(getJugador);
            ps.setInt(1, idJugador);

            ResultSet rs = ps.executeQuery();
            if(!rs.next()){
            	// Nos metemos aquí si la consulta no devuelve nada
            	return null;
            }
            
            
               
                int id = rs.getInt("id");
                String nombre = rs.getString("nombre");
                java.util.Date fechaNac=new java.util.Date(rs.getDate("fechaNacimiento").getTime());
                int idEquipo=rs.getInt("equipo");
                
                

                j.setId(id);
                j.setNombre(nombre);
                j.setFechaNacimiento(fechaNac);
                Equipo e=this.obtenerEquipo(idEquipo);
                j.setEquipo(e);
                

            
            return j;


        } catch (Exception e) {
        	throw new DAOException("Ha habido un problema al obtener el jugador: ",e);
        } finally {
            try {
                ps.close();
                conn.close();
            } catch (SQLException ex) {
            	throw new DAOException("Error al cerrar la base de datos",ex);
            }

        }
        
    }

   
    public Equipo obtenerEquipo(int idEquipo) throws DAOException {
    	//Obtiene de la BD el jugador con id 
        Equipo e= new Equipo();
        Connection conn = null;
        PreparedStatement ps = null;
        
        try {
            conn = DriverManager.getConnection(url, login, password);

            ps = conn.prepareStatement(getEquipo);
            ps.setInt(1, idEquipo);

            ResultSet rs = ps.executeQuery();
            if(!rs.next()){
            	// Nos metemos aquí si la consulta no devuelve nada
            	return null;
            }
            
            
               
                int id = rs.getInt("id");
                String nombre = rs.getString("nombre");
                double presupuesto=rs.getDouble("presupuesto");
                
                

                e.setId(id);
                e.setNombre(nombre);
                e.setPresupuesto(presupuesto);
                
                

            
            return e;


        } catch (Exception ex) {
        	throw new DAOException("Ha habido un problema al obtener el equipo: ",ex);
        } finally {
            try {
                ps.close();
                conn.close();
            } catch (SQLException ex) {
            	throw new DAOException("Error al cerrar la base de datos",ex);
            }

        }
    }

    
    public List<Jugador> obtenerTodosJugadores() throws DAOException {
    	//Obtiene de la BD la lista de todos las jugadores y devuelve dicha lista
        List<Jugador> jugadores = new ArrayList<Jugador>();
        Connection conn = null;
        PreparedStatement ps = null;
        
        try {
            conn = DriverManager.getConnection(url, login, password);

            ps = conn.prepareStatement(getAllJugadores);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Jugador  j = new Jugador();
                int id = rs.getInt("id");
                String nombre = rs.getString("nombre");
                java.util.Date fechaNac=new java.util.Date(rs.getDate("fechaNacimiento").getTime());
                int idEquipo=rs.getInt("equipo");
                Equipo e=this.obtenerEquipo(idEquipo);
                

                j.setId(id);
                j.setNombre(nombre);
                j.setFechaNacimiento(fechaNac);
                j.setEquipo(e);
               
              jugadores.add(j);

            }
            return jugadores;


        } catch (Exception e) {
        	throw new DAOException("Ha habido un problema al obtener la lista de jugadores: ",e);
        } finally {
            try {
                ps.close();
                conn.close();
            } catch (SQLException ex) {
            	throw new DAOException("Error al cerrar la base de datos",ex);
            }

        }
       
    }

    
    public List<Equipo> obtenerTodosEquipos() throws DAOException {
    	//Obtiene de la BD la lista de todos las equipos y devuelve dicha lista
        List<Equipo> equipos = new ArrayList<Equipo>();
        Connection conn = null;
        PreparedStatement ps = null;
        
        try {
            conn = DriverManager.getConnection(url, login, password);

            ps = conn.prepareStatement(getAllEquipos);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Equipo  e = new Equipo();
                int id = rs.getInt("id");
                String nombre = rs.getString("nombre");
                double presupuesto=rs.getDouble("presupuesto");
                

                e.setId(id);
                e.setNombre(nombre);
                e.setPresupuesto(presupuesto);
                
               
              equipos.add(e);

            }
            return equipos;


        } catch (Exception ex) {
        	throw new DAOException("Ha habido un problema al obtener la lista de equipos: ",ex);
        } finally {
            try {
                ps.close();
                conn.close();
            } catch (SQLException ex) {
            	throw new DAOException("Error al cerrar la base de datos",ex);
            }

        }
    }

   
    public void eliminarJugador(int id) throws DAOException {
    	//Elimina un jugador de la BD
        
        Connection conn = null;
        PreparedStatement ps = null;
        
        try {
            conn = DriverManager.getConnection(url, login, password);

            ps = conn.prepareStatement(deleteJugador);
            ps.setInt(1, id);

            ps.executeUpdate();
           


        } catch (Exception ex) {
        	throw new DAOException("Ha habido un problema al eliminar el jugador: ",ex);
        } finally {
            try {
                ps.close();
                conn.close();
            } catch (SQLException ex) {
            	throw new DAOException("Error al cerrar la base de datos",ex);
            }

        }
    }

	
	public void finalizar() {
		// Echa un vistazo a c�mo hemos implementado los m�todos, �Cu�ndo se abre  y se cierra la conexi�n a la bd?
		// Otra opci�n  ser�a abrir la conexi�n en el constructory cerrarla aqu�
		//como ejercicio puedes cambiar la implementaci�n, pero tal y como est� hecho aqu� no hay ning�n c�digo
		
	}
    
}
