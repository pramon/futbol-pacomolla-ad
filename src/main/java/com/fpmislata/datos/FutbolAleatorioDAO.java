/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fpmislata.datos;

import com.fpmislata.entidades.Equipo;
import com.fpmislata.entidades.Jugador;

import java.util.List;
import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;

/**
 *
 * @author rafael
 */
public class FutbolAleatorioDAO implements IFutbolDAO {
	// Para ESTA implementación vamos a suponer que el nombre del Jugador y del
	// Equipo
	// no puede ser superior a 40 caracteres, y la fecha son dd/MM/yyyy 10
	// caracteres
	// así el jugador ocupará id -> 4 bytes idEquipo-> 4 bytes nombre-> 40*2=
	// 80 Bytes y fecha nacimiento= 2*10= 20 Bytes
	// total jugador 108 Bytes
	// Para el equipo Id -> 4 bytes nombre 80 Bytes y presupuesto 8 Bytes Total
	// 92 Bytes
	// ¿Y si mañana decido que necesito 50 caracteres para el nombre del
	// jugador? En ese caso bastará con cambiar esta única clase
	// el resto del código permanece intacto,

	File ficheroJugadores = null;
	File ficheroEquipos = null;

	public FutbolAleatorioDAO() throws DAOException {
		try {
			Properties pro = new Properties();
   		 	pro.load(this.getClass().getResourceAsStream("/configuracion.properties"));
   		 	String ficJugadores=pro.getProperty("jugadoresAleatorio");
   		 	String ficEquipos=pro.getProperty("equiposAleatorio");
   		 	this.ficheroEquipos= new File(ficEquipos);
   		 	this.ficheroJugadores= new File(ficJugadores);
			
		
		} catch (NullPointerException ex) {
			throw new DAOException(
					"No ha sido posible abrir el archivo de acceso aleatorio",ex);
		} catch (IOException e) {
			throw new DAOException(
					"Eror al acceder al archivo de configuración",e);
		}

	}

	
	public void nuevoJugador(Jugador j) throws DAOException {
		RandomAccessFile file = null;
		try {
			file = new RandomAccessFile(ficheroJugadores, "rw");
			// calculamos la posición el jugador 1 debe situarse en la 0 es
			// decir la id -1
			// El jugador 2 en la 108 es decir la posicion es (id-1)*108
			int posicion = (j.getId() - 1) * 108;
			file.seek(posicion);// Me situo en la posición adecuada
			file.writeInt(j.getId());

			// Inserto el nombre
			StringBuffer buffer = null;
			buffer = new StringBuffer(j.getNombre());
			buffer.setLength(40);
			file.writeChars(buffer.toString());

			// Inserto la fecha nacimiento, me aseguro que tiene el formato
			// dd/MM/yyyy
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			String fechaStr = sdf.format(j.getFechaNacimiento());
			StringBuffer buffer2 = null;
			buffer2 = new StringBuffer(fechaStr);
			buffer2.setLength(10);
			file.writeChars(buffer2.toString());

			// Por último inserto el id de equipo
			file.writeInt(j.getEquipo().getId());

		} catch (FileNotFoundException ex) {
			throw new DAOException("No existe el Fichero aleatorio.", ex);
		} catch (IOException ex2) {
			throw new DAOException("No se ha podido insertar el jugador: ", ex2);
		} finally {
			try {
				if(file!=null){file.close();}
			} catch (IOException e) {
				throw new DAOException("Error al cerrar el fichero: ", e);
			}
		}
	}

	
	public void nuevoEquipo(Equipo e) throws DAOException {
		RandomAccessFile file = null;
		try {
			file = new RandomAccessFile(ficheroEquipos, "rw");
			// calculamos la posición el equipo 1 debe situarse en la 0 es
			// decir la id -1
			// El equipo 2 en la 92 es decir la posicion es (id-1)*92
			int posicion = (e.getId() - 1) * 92;
			file.seek(posicion);// Me situo en la posición adecuada
			file.writeInt(e.getId());

			// Inserto el nombre
			StringBuffer buffer = null;
			buffer = new StringBuffer(e.getNombre());
			buffer.setLength(40);
			file.writeChars(buffer.toString());

			// Por último inserto el presupuesto
			file.writeDouble(e.getPresupuesto());

		} catch (FileNotFoundException ex) {
			throw new DAOException("No existe el Fichero aleatorio.", ex);
		} catch (IOException ex2) {
			throw new DAOException("No se ha podido insertar el equipo: ", ex2);
		} finally {
			try {
				if(file!=null){file.close();}
			} catch (IOException ex3) {
				throw new DAOException("Error al cerrar el fichero: ", ex3);
			}
		}

	}

	
	public Jugador obtenerJugador(int idJugador) throws DAOException {
		RandomAccessFile file = null;
		try {
			file = new RandomAccessFile(ficheroJugadores, "r");
			Jugador j = null;
			int idEquipo, posicion;
			char nombreArray[] = new char[40], aux;
			char fechaArray[] = new char[10];

			posicion = (idJugador - 1) * 108;
			file.seek(posicion);
			int idPrueba = file.readInt();
			if (idPrueba == 0) {
				file.close();
				return null;
			}
			// Leo el nombre
			for (int i = 0; i < nombreArray.length; i++) {
				aux = file.readChar();// recorro uno a uno los caracteres del
										// nombre
				nombreArray[i] = aux; // los voy guardando en el array
			}
			String nombre = new String(nombreArray);
			// Leo la fecha
			for (int i = 0; i < fechaArray.length; i++) {
				aux = file.readChar();// recorro uno a uno los caracteres del
										// nombre
				fechaArray[i] = aux; // los voy guardando en el array
			}
			String fechaStr = new String(fechaArray);
			// ahora paso la fecha a date
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			Date fecha = sdf.parse(fechaStr);

			// Ahora el id de Equipo
			idEquipo = file.readInt();
			Equipo e = this.obtenerEquipo(idEquipo);
			j = new Jugador();
			j.setId(idJugador);
			j.setNombre(nombre);
			j.setFechaNacimiento(fecha);
			j.setEquipo(e);

			return j;

		} catch (FileNotFoundException ex) {
			throw new DAOException("No existe el Fichero aleatorio.", ex);
		} catch (IOException ex2) {
			throw new DAOException("No se ha podido insertar el jugador: ", ex2);

		} catch (ParseException e1) {
			throw new DAOException("La fecha no esta en el formato correcto.",
					e1);
		} finally {
			try {
				if(file!=null){file.close();}
			} catch (IOException ex3) {
				throw new DAOException(
						"Error al cerrar el fichero aleatorio: ", ex3);
			}
		}
	}

	
	public Equipo obtenerEquipo(int idEquipo) throws DAOException {
		RandomAccessFile file = null;
		try {
			file = new RandomAccessFile(ficheroEquipos, "r");
			Equipo e = null;
			int posicion;
			char nombreArray[] = new char[40], aux;
			double presupuesto;

			posicion = (idEquipo - 1) * 92;
			file.seek(posicion);
			int idPrueba = file.readInt();
			if (idPrueba == 0) {
				
				return null;
			}
			// Leo el nombre
			for (int i = 0; i < nombreArray.length; i++) {
				aux = file.readChar();// recorro uno a uno los caracteres del
										// nombre
				nombreArray[i] = aux; // los voy guardando en el array
			}
			String nombre = new String(nombreArray);

			// Ahora el presupuesto
			presupuesto = file.readDouble();
			e = new Equipo();
			e.setId(idEquipo);
			e.setNombre(nombre);
			e.setPresupuesto(presupuesto);

			return e;

		} catch (FileNotFoundException ex) {
			throw new DAOException("No existe el Fichero aleatorio.", ex);
		} catch (IOException ex2) {
			throw new DAOException("No se ha podido obtener el equipo.", ex2);
		} finally {
			try {
				if(file!=null){file.close();}
			} catch (IOException ex3) {
				throw new DAOException(
						"Error al cerrar el fichero aleatorio: ", ex3);
			}
		}

	}

	
	public List<Jugador> obtenerTodosJugadores() throws DAOException {
		List<Jugador> jugadores = new ArrayList<Jugador>();
		RandomAccessFile file = null;

		try {
			file = new RandomAccessFile(ficheroJugadores, "r");
			Jugador j = null;
			int idEquipo;
			long posicion;
			char nombreArray[] = new char[40], aux;
			char fechaArray[] = new char[10];

			posicion = 0;
			file.seek(posicion);
			for (;;) {
				int id = file.readInt();
				if (id == 0) {
					// Si no existe paso el cursor al siguiente registro
					posicion = file.getFilePointer() + 104; // 108 - 4 que es la
															// longitud del
															// entero que ya
															// hemos leido
					file.seek(posicion); // poasamos al siguiente regist
				} else { // esto solo lo ejecuto si hay datos
					// Leo el nombre
					for (int i = 0; i < nombreArray.length; i++) {
						aux = file.readChar();// recorro uno a uno los
												// caracteres del nombre
						nombreArray[i] = aux; // los voy guardando en el array
					}
					String nombre = new String(nombreArray);
					// Leo la fecha
					for (int i = 0; i < fechaArray.length; i++) {
						aux = file.readChar();// recorro uno a uno los
												// caracteres del nombre
						fechaArray[i] = aux; // los voy guardando en el array
					}
					String fechaStr = new String(fechaArray);
					// ahora paso la fecha a date
					SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
					Date fecha = sdf.parse(fechaStr);

					// Ahora el id de Equipo
					idEquipo = file.readInt();
					Equipo e = this.obtenerEquipo(idEquipo);
					j = new Jugador();
					j.setId(id);
					j.setNombre(nombre);
					j.setFechaNacimiento(fecha);
					j.setEquipo(e);

					jugadores.add(j);
				}// cierro el else
					// Si he recorrido todos los bytes salgo del for
				if (file.getFilePointer() == file.length())
					break;
			}

			return jugadores;

		} catch (FileNotFoundException ex) {
			throw new DAOException("No existe el Fichero aleatorio.", ex);

		} catch (IOException ex2) {
			return null;
		} catch (ParseException e1) {
			throw new DAOException("Error en el formato de fecha", e1);
		} finally {
			try {
				if(file!=null){file.close();}
				
			} catch (IOException ex3) {
				throw new DAOException(
						"Error al cerrar el fichero aleatorio: ", ex3);
			}
		}
	}

	
	public List<Equipo> obtenerTodosEquipos() throws DAOException {
		List<Equipo> equipos = new ArrayList<Equipo>();
		RandomAccessFile file=null;
		try {
			file = new RandomAccessFile(ficheroEquipos, "r");
			Equipo e = null;
			long posicion;
			char nombreArray[] = new char[40], aux;
			double presupuesto;

			posicion = 0;
			file.seek(posicion);
			for (;;) {
				int id = file.readInt();
				if (id == 0) { // si el registro está vacio
					posicion = file.getFilePointer() + 88; // 92 - 4 que es la
															// longitud del
															// entero que ya
															// hemos leido
					file.seek(posicion); // poasamos al siguiente regist
				} else {
					// Leo el nombre solo en el caso de que no sea un registro
					// vacio
					for (int i = 0; i < nombreArray.length; i++) {
						aux = file.readChar();// recorro uno a uno los
												// caracteres del nombre
						nombreArray[i] = aux; // los voy guardando en el array
					}
					String nombre = new String(nombreArray);

					// Ahora el presupuesto
					presupuesto = file.readDouble();
					e = new Equipo();
					e.setId(id);
					e.setNombre(nombre);
					e.setPresupuesto(presupuesto);
					equipos.add(e);
				} // cierro el else
				if (file.getFilePointer() == file.length())
					break;
			}
			
			return equipos;

		} catch (FileNotFoundException ex) {
			throw new DAOException("No existe el Fichero aleatorio.",ex);
		} catch (IOException ex2) {
			throw new DAOException("No se ha podido obtener los equipos desde el fichero aleatorio.",ex2);
		}
		finally {
			try {
				if(file!=null){file.close();}
			} catch (IOException ex3) {
				throw new DAOException(
						"Error al cerrar el fichero aleatorio: ", ex3);
			}
		}
	}

	
	public void eliminarJugador(int id) throws DAOException {
		RandomAccessFile file=null;
		try {
		    file = new RandomAccessFile(ficheroJugadores, "rw");
			long posicion = (id - 1) * 108;
			file.seek(posicion);
			file.writeInt(0);
			
		} catch (IOException e) {
			throw new DAOException("No se ha podido eliminar el jugador del archivo aleatorio: ",e);
		}
		finally {
			try {
				if(file!=null){file.close();}
			} catch (IOException ex3) {
				throw new DAOException(
						"Error al cerrar el fichero aleatorio: ", ex3);
			}
		}
		
	}

	

}
