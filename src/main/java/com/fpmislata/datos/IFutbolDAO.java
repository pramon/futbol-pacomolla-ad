/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.fpmislata.datos;

import com.fpmislata.entidades.Equipo;
import com.fpmislata.entidades.Jugador;

import java.util.List;

/**
 *
 * @author rafael
 */
public interface IFutbolDAO {
    public void nuevoJugador(Jugador j) throws DAOException;
    public void nuevoEquipo(Equipo e) throws  DAOException;
    public void eliminarJugador(int id) throws  DAOException;
    public Jugador obtenerJugador(int idJugador) throws  DAOException;
    public Equipo obtenerEquipo(int idEquipo) throws  DAOException;
    public List<Jugador> obtenerTodosJugadores() throws  DAOException;
    public List<Equipo> obtenerTodosEquipos() throws  DAOException;
  
    
    
}
