package com.fpmislata.datos;



import static org.junit.Assert.*;

import java.io.IOException;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.mysql.MySqlDataTypeFactory;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.Assert; 



import com.fpmislata.entidades.Equipo;
import com.fpmislata.entidades.Jugador;
import com.mysql.jdbc.Connection;




public class FutbolJDBCDAOTest {
	
	//Fíjate son variables static de clase
	private static String bd = null;
	private static String login = null;
	private static String password = null;
	private static String url =null;
	
	
	private static IDatabaseConnection connection;
	private static IDataSet dataset;
	private static IFutbolDAO idao=null;
	
	
	//Este método se ejucuta UNA vez, justo antes de empezar las pruebas
	@BeforeClass
	public static void inicializarBD() throws Exception {
		

        try {
       	 //Ojo esta es una clase de test obtendrá la configuración
        //del .properties de src/TEST/resources NO de src/main/resources
       	 Properties pro = new Properties();
   		 pro.load(FutbolJDBCDAOTest.class.getResourceAsStream("/configuracion.properties"));
   		 bd=pro.getProperty("bdJDBC");
   		 login=pro.getProperty("loginJDBC");
   		 password=pro.getProperty("passwordJDBC");
   		 url= "jdbc:mysql://localhost/" + bd;

            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection jdbcConnection = (Connection) DriverManager
 	               .getConnection(url,login,password);
            connection = new DatabaseConnection(jdbcConnection);
            

            // establecemos en dbunit que se ttrata de una bd de pysql, si no da un warning
            DatabaseConfig dbConfig = connection.getConfig();
            dbConfig.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new MySqlDataTypeFactory());

       	 
	         FlatXmlDataSetBuilder flatXmlDataSetBuilder = new FlatXmlDataSetBuilder();
	         flatXmlDataSetBuilder.setColumnSensing(true);
	         dataset = flatXmlDataSetBuilder.build(Thread.currentThread()
	               .getContextClassLoader()
	               .getResourceAsStream("test-dao-dataset.xml"));
	         //test-dao-dataset debe de estar en src/test/resources
	       
            //Al ejecutar desde aquí la instucción del new 
	         //en el constructor coge la configuración de la bd de los recursos de test
	         idao= new FutbolJDBCDAO();
            

        } catch (InstantiationException ex) {
        	
       	 	System.out.println("Hubo un problema al iniciar la base de datos del test: ");
       	 	ex.printStackTrace();
        } catch (IllegalAccessException iae) {
       	 
        	System.out.println("Hubo un problema al acceder a la base de datos. del test");
        	iae.printStackTrace();
        } catch (SQLException sqle) {
        	System.out.println("Hubo un problema al intentar conectarse con la base de datos "+sqle);
        	sqle.printStackTrace();
            
        } catch (ClassNotFoundException cnfe) {
        	System.out.println("No se encuentra el driver JDBC."+cnfe);
        	cnfe.printStackTrace();
        } catch (IOException ioe) {
        	System.out.println("Error al accerder al archivo de configuración: "+ioe);
        	ioe.printStackTrace();
		}
        catch (Exception ex) {
        	
        	System.out.println("Error inesperado al iniciar los test");
        	ex.printStackTrace();
	      }
	}
	
	//Este método se ejecuta Una vez al finalizar todos los test
	@AfterClass
	public static  void finalizar() throws Exception{
		if(connection!=null){
			connection.close();
		}
	}
	
	
	//Este método se ejecuta tantas veces como test haya, es decir, 
	//Se ejecuta antes de ejecutar cada uno de los métodos etiquetados como @test
	@Before
	public void setUp() throws Exception{
		DatabaseOperation.CLEAN_INSERT.execute(connection, dataset);
		
	}
	
	//Este método se ejecuta Después de ejeucutar cada uno de los test
	//En este caso no tenemos que hacer nada porque la bd se pone en estado conocido al iniciar
	
	@After
	public void tearDown() throws Exception {
	}
	
	
	//Anotamos con @Test todos los test que queramos hacer, generalmnte uno
	//por método.Antes de iniciar el test la bd está en un estado conocido
	//es decir la base de datos del test está tal y como la ha configurado el dataset-dao.xml
	//tras su ejecución por parte de dbunit
	//Estado Inicial 3 equipos  con 5 jugadores
	//1 Real Madrid 1Cristano 25/3/1998
	//				2Bale	
	//				3Keylor
	//2 FC Barcelona
	//				4 Paquete n1 2/2/1995
	//				5 Paquete n2 4/3/1997
	//3 Valenca CF
	@Test
	public void testObtenerTodosEquipos() {
		try{
			List<Equipo> equipos= idao.obtenerTodosEquipos();
			
			assertTrue(equipos.size() == 3);
			
			Collections.sort(equipos);
			Equipo e= equipos.get(0);
			assert(e.getId()==1);
			assert(e.getNombre().equals("Real Madrid"));
			assert(e.getPresupuesto()==1250000);
			//Los otros equipos también podriamos probarlos
			
			
			
			
			
			
			
			}catch(Exception e){
				fail("TEST NO SUPERADO: No se ha podido obtener la lista  de equipos al producirse una excepcion: "+e.getLocalizedMessage());
			}
	}
	
	//El orden de los test NO importa, Junit los puede ejecutar cada vez de una manera
	@Test
	public void testNuevoEquipo() {
		try{
		Equipo e1=new Equipo();
		e1.setId(4);
		e1.setNombre("Sevilla CF");
		e1.setPresupuesto(10000);
		idao.nuevoEquipo(e1);
		
		
		List<Equipo> equipos=idao.obtenerTodosEquipos();
		assert(equipos.size()==4);
		
		//En principio cuando testeamos un método no es aconsejable usar otro (obtenerEquipos)
		//pero en este caso va a ser nuestra mejor opción, la consecuencia es que:
		// si obtenerTodosEquipos está mal el test dará error aunque nuevo equipo esté bien
		//En cualquier caso si testNuevoEquipo no da error tendremos la total certeza de que el método está correcto.
		//El caso es como si tuvieramos una alarma demasiado sensible, puede que suene sin que ocurra nada, pero
		//si no suena seguro que no hay problema. 
		//En conclusión, si vemos que falla el test de obtenerTodosEquipos junto con otros, empezaremos por resolver 
		//la incidencia de obtenerTodosEquipos, puede ser que una vez arreglado funcionen ya todos
		
		
		//ahora comprobamos que el equipo está correcto
		Collections.sort(equipos);
		Equipo e2=equipos.get(3);
		assert(e2.getId()==4);
		assert(e2.getNombre().equals("Sevilla CF"));
		assert(e2.getPresupuesto()==10000);
		
		
		
		}catch(Exception e){
			fail("TEST NO SUPERADO: No se han insertado el equipo al producirse una excepcion: "+e.getLocalizedMessage());
		}
	}
	
	
	
	@Test
	public void testObtenerTodosJugadores() {
		try{
			List<Jugador> jugadores= idao.obtenerTodosJugadores();
			assert(jugadores.size()==5);
			//Comprobamos que el equipo NO es nulo
			//Con este test ¡cuantos NullPointerException nos hubieramos ahorrado!!
			for(Jugador j :jugadores){
				assertNotNull(j.getEquipo());
			}
			
			//vamos a comprobar también que 
			//los datos obtenidos son correctos, lo vamos a hacer solo para un jugador
			//hacerlo para todos seria un poco redundante, aunque se podría hacer también 
			
			//Voy a usar aqui otra forma de  encontrar el elemento de la lista
			//en lugar de ordenar por id . Os será util para el id de grupo que es un String
			//por supuesto se podría usar el for de antes para ser más eficientes
			Jugador j2=null;
			for(Jugador j :jugadores){
				if(j.getId()==2){
					j2=j;
				}
			}
			assertNotNull(j2);
			assert(j2.getNombre().equals("Gareth Bale"));
			SimpleDateFormat sdf= new  SimpleDateFormat("dd/MM/yyyy");
			//Ojo fíjate que en el dataset el formato para fechas es 
			//distino yyyy-MM-dd
			Date fecha=sdf.parse("15/3/1995");
			assert(j2.getFechaNacimiento().getTime()==fecha.getTime());
			assert(j2.getEquipo().getId()==1);
			
			
			
			
			}catch(Exception e){
				fail("TEST NO SUPERADO: No se ha podido obtener la lista vacia  de jugadores al producirse una excepcion: "+e.getLocalizedMessage());
			}
	}
	
	@Test
	public void testNuevoJugador() {
		try{
			//Aqui tambien probamos getEquipo y obtener jugadores, es decir que si falla
			//algunos de estos métodos tambien fallará este
			SimpleDateFormat sdf= new SimpleDateFormat("dd/MM/yyyy");
			

			Jugador j1= new Jugador();
			Date d1=sdf.parse("2/1/1998");
			j1.setId(7);
			j1.setNombre("Sergio Ramos");
			j1.setFechaNacimiento(d1);
			Equipo e=idao.obtenerEquipo(1);
			j1.setEquipo(e);
			idao.nuevoJugador(j1);
			
			List<Jugador> jugadores=idao.obtenerTodosJugadores();
			assert(jugadores.size()==6);
			Jugador j3=null;
			for(Jugador j :jugadores){
				if(j.getId()==7){
					j3=j;
				}
			}
			assertNotNull(j3);
			assert(j3.getNombre().equals("Sergio Ramos"));
			assert(j3.getEquipo().getId()==1);
			assert(j3.getFechaNacimiento().getTime()==d1.getTime());
			
					
			
			}catch(Exception e){
				fail("TEST NO SUPERADO: No se han insertado el jugador al producirse una excepcion: "+e.getLocalizedMessage());
			}
	}

	
	
	@Test
	public void testObtenerJugador() {
		
		//Otra posible forma de implementar los test
		//Usando fail(mensaje), puede ser inteeresante para
		//obtener un mensaje de error específico
		try{
			
			SimpleDateFormat sdf= new SimpleDateFormat("dd/MM/yyyy");
			Date d1=sdf.parse("25/3/1998");
			
			
			Jugador ju2=idao.obtenerJugador(1);
			if(ju2.getNombre()==null){
				fail("No se ha obtenido el jugador apropiadamente, el jugador no tiene nombre");
			}
			if(ju2.getFechaNacimiento()==null){
				fail("No se ha obtenido el jugador apropiadamente, el jugador no tiene fecha de nacimiento");
			}
			if(ju2.getEquipo()==null){
				fail("No se ha obtenido el jugador apropiadamente, el jugador no tiene equipo");
			}
			
			if(!(ju2.getId()==1)){
				fail("No se ha obtenido el jugador apropiadamente, el id no es correcto");
			}
			if(!(ju2.getNombre().equals("Cristiano Ronaldo"))){
				fail("No se ha obtenido el jugador apropiadamente, el jugador no tiene el nombre correcto");
			}
			if(!(ju2.getFechaNacimiento().getTime()==d1.getTime())){
				fail("No se ha obtenido el jugador apropiadamente, el jugador no tiene la fecha de nacimiento correcta");
			}
			if(!(ju2.getEquipo().getId()==1)){
				fail("No se ha obtenido el jugador apropiadamente, el jugador no tiene el equipo correcto");
			}
			
			
			}catch(Exception e){
				fail("TEST NO SUPERADO: No se ha podido obtener la lista de jugadores al producirse una excepcion: "+e.getLocalizedMessage());
			}
	}
	

	@Test
	public void testObtenerEquipo() {
		try{
			Equipo e=idao.obtenerEquipo(1);
			assert(e.getId()==1);
			assert(e.getNombre().equals("Real Madrid"));
			assert(e.getPresupuesto()==1250000);
			
			
			
			
			}catch(Exception e){
				fail("TEST NO SUPERADO: No se ha podido obtener la lista de equipos al producirse una excepcion: "+e.getLocalizedMessage());
			}
	}

	
	

	@Test
	public void testEliminarJugador() {
		try{
		//Antes de empezar a comprobar 
		//Por qué no se pasa este test (en caso de que no lo haga
		//Nos aseguraremos de que ObternerTodosJugador y obtenerJugador funciona bien
		
		List<Jugador> jugadores=null;
		
		idao.eliminarJugador(2);
		jugadores=idao.obtenerTodosJugadores();
		if(!(jugadores.size()==4)){
			fail("TEST NO SUPERADO: No se ha eliminado el jugador");
		}
		if(idao.obtenerJugador(2)!=null){
			fail("TEST NO SUPERADO: No se ha eliminado el jugador Seleccionado");
		}
		
		
		
		}catch(Exception e){
			fail("TEST NO SUPERADO: No se ha podido obtener la lista de jugadores al producirse una excepcion: "+e.getLocalizedMessage());
		}
	}

}
